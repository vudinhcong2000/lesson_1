/*--- setTimeout and setInterval---*/
//Begin:

//       - setTimeout: chạy duy nhất một lần sau một khoảng thời gian
//       - setInterval: thực thi liên tục sau một khoảng thời gian
// Vd :
// khai báo biến
// var fullName = "Vu Dinh Cong"
// setTimeout(function() {
//    alert("fullName");
// }, 1000)

// setInterval(function() {
//       // alert("fullName");
//       console.log(fullName + Math.random());
// }, 1000)

// End: setTimeout and setInterval

/* --- Toán tử ---*/
//Begin
function myFunction() {
  // khai báo biến
  var a = document.querySelector("#soA").value;
  var b = document.querySelector("#soB").value;
  var calculation = document.querySelector("#calculation").value;
  var resualt = 0;

  // điều kiện
  if (calculation == "+") {
    resualt = parseInt(a) + parseInt(b);
  } else if (calculation == "-") {
    resualt = parseInt(a) - parseInt(b);
  } else if (calculation == "*") {
    resualt = parseInt(a) * parseInt(b);
  } else if (calculation == "/") {
    resualt = parseInt(a) / parseInt(b);
  } else {
    resualt = parseInt(a) % parseInt(b);
  }

  document.querySelector("#ketqua").value = resualt;
}
//End: Toán tử

/* --- Kiểu dữ liệu ---*/
//Begin

//   1. Dữ liệu nguyên thủy - Primitive Data
//      - Number: các số bất kỳ loại nào: số nguyên hoặc dấu phẩy động.
//      - String: các số bất kỳ loại nào: số nguyên hoặc dấu phẩy động.
//      - Boolean(kiểu logic): gồm 2 giá trị true và false
//      - Null: các giá trị không xác định – một loại độc lập có một giá trị duy nhất null.
//      - Underfined: các giá trị chưa được gán – một kiểu độc lập có một giá trị duy nhất undefined.
//      - Symbol:
//   2. Dữ liệu phức tạp - Comlex Data
//      - Function
//      - Object
// Ví dụ

//Number type
// var b = 2;
// var a = 1;

// console.log(typeof a);

//String type
// var fullName = 'Vũ Đình Công';

//  console.log(typeof fullName)

//Boolean type
//  var isSuccess = true;

//       console.log(typeof isSuccess)

// Undefined type
// var name

// Null
// var isNull = null

// Symbol
// var id = Symbol('id');

// Function
// var myFunction = function() {
//       alert('Vũ Đình Công');
// }

// Object type
// var myObject = {
//       name: 'Cong',
//       age: 21,
//       adress: 'Hà Nội',
//       myFunction: function () {

//       }
// };

// var myArray = [
//       'Javascript',
//       'Php',
//       'python'
// ];

// End

/* --- Câu lệnh rẽ nhánh - If else switch ---*/
//Begin
//---- If else----

// var date = 9;

// if (date === 2) {
//       console.log('Hôm nay là thứ 2');
// } else if (date === 3) {
//       console.log('Hôm nay là thứ 3');
// } else if (date === 4) {
//       console.log('Hôm nay là thứ 4');
// } else {
//       console.log('Không biết là thứ mấy');
// }

//---- switch----

// var date = 3;

// switch(date) {
//       case 2:
//             console.log('Hôm nay là thứ 2');
//             break;
//       case 3:
//             console.log('Hôm nay là thứ 3');
//             break;
//       case 4:
//             console.log('Hôm nay là thứ 4');
//             break;
//       case 5:
//             console.log('Hôm nay là thứ 5');
//             break;
//       default:
//             console.log('Không biết');
// }
//End

/* --- Vòng lặp ---*/
//Begin
// For - loop
// - for: lặp với điều kiện đúng
// - for/in: Lặp qua key của đối tượng
// - for/of: Lặp qua value của đối tượng
// - while: Lặp khi điều kiện đúng
// - do/while: Lặp ít nhất 1 lần, sau đó lặp khi điều kiện đúng

//for loop---

// var myArray = [
//       'Javascript',
//       'PHP',
//       'Java',
//       'Dart',
//       'python'
// ];

// var arrayLength = myArray.length;

// for (var i = 0; i < arrayLength; i++) {
//       console.log(myArray[i]);
// }

//For/in loop
// var myInfo = {
//       name: 'Vu Dinh Cong',
//       age: 21,
//       address: 'Ha Noi'
// }

// for (var key in myInfo) {
//       console.log(myInfo[key]);
// }

//For/of loop : không sử dụng được với object thường dúng muốn lấy những phần tử của một mảng những chữ cái của một chuỗi
//đối với mảng
// var languages = [
//       'Javescript',
//       'PHP',
//       'Java'
// ];

// đối với chuỗi
// var languages = 'Javescript';

//đối với object
// var myInfo = {
//       name: 'Vu Dinh Cong',
//       age: 21,
//       address: 'Ha Noi'
// };

// for (var value of Object.keys(myInfo)) {
//       console.log(myInfo[value]);
// }

//While loop
// var myArray = ["Javascript", "PHP", "python"];

// var i = 0;
// var languages = myArray.length;
// while (i < languages) {
//   console.log(myArray[i]);
//   i++;
// }

//do/while loop
// var i = 0;

// do {
//   i++;

//   console.log(i);
// } while (i < 10);

// Vòng lặp lồng nhau - Nested loop
// var myArray = [
//       [1, 2],
//       [3, 4],
//       [5, 6]
// ];

// for (var i = 0; i<myArray.length; i++) {
//       for (var j = 0; j<myArray[i].length; j++) {
//             console.log(myArray[i][j]);
//       }
// }
//End
